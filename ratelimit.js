import {
  BehaviorSubject,
  distinctUntilChanged,
  firstValueFrom,
  switchMap,
  timer,
} from "rxjs";

export const rateLimit$ = new BehaviorSubject(0).pipe(distinctUntilChanged());
export const rateLimit = () =>
  firstValueFrom(rateLimit$.pipe(switchMap((delayUntil) => timer(delayUntil))));

// round ts up to the next closest period
const next = (period) => (ts) => Math.ceil(ts / period) * period;

// find start of next 15 min period
const nextQuarter = next(15 * 60 * 1000);
// find start of next 24 h period
const nextMidnight = next(24 * 60 * 60 * 1000);

export function checkRateLimit(headers) {
  const date = new Date(headers["date"]).getTime();
  const [quarterLimit, dailyLimit] = headers["x-ratelimit-limit"]
    .split(",")
    .map(Number);
  const [quarterUsage, dailyUsage] = headers["x-ratelimit-usage"]
    .split(",")
    .map(Number);

  if (
    isNaN(date) ||
    isNaN(quarterLimit) ||
    isNaN(dailyLimit) ||
    isNaN(quarterUsage) ||
    isNaN(dailyUsage)
  )
    throw new Error(`Invalid ratelimit headers`);

  // check daily usage first!
  if (dailyUsage >= dailyLimit)
    return rateLimit$.next(new Date(nextMidnight(date)));

  if (quarterUsage >= quarterLimit)
    return rateLimit$.next(new Date(nextQuarter(date)));

  rateLimit$.next(0);
}
