## Description

A simple axios wrapper for the Strava API, that does two things

- Automatically refreshes the user's access token when it expires

- Parses the rate limit information in the response headers, exposing elements enabling you to control API usage.

## Configuration

All configuration is done through environment variables, which can be declared in an `.env` file.

```
STRAVA_CLIENT_ID=81314
STRAVA_CLIENT_SECRET=61d995466ab1b18e7c8f461416ff278c31b4c680
ACCESS_TOKEN=a0db11e87c0eb089a18f4d51edbeca7af38965ba
REFRESH_TOKEN=08164631109d14174388a69f46b65d5de571986e
```

The user should be authorized with the activity:read scope.

## Example application

A very basic application that uses the API wrapper to list all the user's activities. It automatically pauses whenever the application is rate limited, and will automatically resume as soon as the rate limit usage has been reset.

```
$ node -r dotenv/config test/example.js
resuming @ Thu Dec 09 2021 18:11:17 GMT+0200 (South Africa Standard Time)
Run
Run
Run
.
.
.
pausing until Thu Dec 09 2021 18:15:00 GMT+0200 (South Africa Standard Time)
Run
resuming @ Thu Dec 09 2021 18:15:01 GMT+0200 (South Africa Standard Time)
Yoga
Run
Walk
.
.
.
pausing until Fri Dec 10 2021 02:00:00 GMT+0200 (South Africa Standard Time)
Yoga
resuming @ Fri Dec 10 2021 02:00:04 GMT+0200 (South Africa Standard Time)
Run
Yoga
Run
.
.
.
pausing until Fri Dec 10 2021 02:15:00 GMT+0200 (South Africa Standard Time)
Walk
resuming @ Fri Dec 10 2021 02:15:01 GMT+0200 (South Africa Standard Time)
Run
Run
Run
.
.
.
pausing until Fri Dec 10 2021 02:30:00 GMT+0200 (South Africa Standard Time)
Workout
resuming @ Fri Dec 10 2021 02:30:01 GMT+0200 (South Africa Standard Time)
Workout
Run
Run
.
.
.
pausing until Fri Dec 10 2021 02:45:00 GMT+0200 (South Africa Standard Time)
Yoga
resuming @ Fri Dec 10 2021 02:45:01 GMT+0200 (South Africa Standard Time)
Run
Run
Workout
.
.
.
pausing until Fri Dec 10 2021 03:00:00 GMT+0200 (South Africa Standard Time)
Walk
resuming @ Fri Dec 10 2021 03:00:01 GMT+0200 (South Africa Standard Time)
Walk
Walk
Walk
.
.
.
pausing until Fri Dec 10 2021 03:15:00 GMT+0200 (South Africa Standard Time)
Workout
resuming @ Fri Dec 10 2021 03:15:01 GMT+0200 (South Africa Standard Time)
Run
Workout
Run
.
.
.
pausing until Fri Dec 10 2021 03:30:00 GMT+0200 (South Africa Standard Time)
Run
resuming @ Fri Dec 10 2021 03:30:01 GMT+0200 (South Africa Standard Time)
Run
Ride
Run
.
.
.
pausing until Fri Dec 10 2021 03:45:00 GMT+0200 (South Africa Standard Time)
Run
resuming @ Fri Dec 10 2021 03:45:01 GMT+0200 (South Africa Standard Time)
Yoga
Run
Run
.
.
.
pausing until Fri Dec 10 2021 04:00:00 GMT+0200 (South Africa Standard Time)
Run
resuming @ Fri Dec 10 2021 04:00:01 GMT+0200 (South Africa Standard Time)
Run
Run
.
.
.
pausing until Fri Dec 10 2021 04:15:00 GMT+0200 (South Africa Standard Time)
Run
resuming @ Fri Dec 10 2021 04:15:01 GMT+0200 (South Africa Standard Time)
Run
Workout
Workout
.
.
.
pausing until Sat Dec 11 2021 02:00:00 GMT+0200 (South Africa Standard Time)
Run

```
