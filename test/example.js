import { createAPI, rateLimit, rateLimit$ } from "../index.js";

const strava = createAPI({
  client_id: process.env.STRAVA_CLIENT_ID,
  client_secret: process.env.STRAVA_CLIENT_SECRET,
});

// create simple user store, tokens are read from environment vars

const user = {
  get access_token() {
    return process.env.ACCESS_TOKEN;
  },
  set access_token(token) {
    process.env.ACCESS_TOKEN = token;

    // also persist token to storage here ...
  },
  get refresh_token() {
    return process.env.REFRESH_TOKEN;
  },
};

const client = strava.client(user);

// subscribe to observable for informational messages ...

rateLimit$.subscribe((delayUntil) =>
  console.log(
    delayUntil ? `pausing until ${delayUntil}` : `resuming @ ${new Date()}`
  )
);

// simple async generator to loop through all activities

async function* getActivities() {
  for (let page = 1; ; page++) {
    // promise will resolve when rate limit usage has been reset
    await rateLimit();
    const { data } = await client.get("/athlete/activities", {
      params: {
        page,
        per_page: 1, // small page size so that it is easier to hit the rate limit
      },
    });

    if (data.length) {
      yield* data;
    } else return;
  }
}

let count = 0;
for await (const activity of getActivities()) {
  console.log(activity.type);
  count++;
}

console.log(`Fetched ${count} activities`);
